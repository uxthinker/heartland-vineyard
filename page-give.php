<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<section class="wrapper section-dark">
<div class="container">
		<div class="row">
			<div class="col">
				<div class="card card-dark">
					<a href="https://secure.subsplash.com/ui/access/M8277H/ute_H-kP_ft_6ZxUNU2kGGI7Y9S32T1YP0ZM5d1UPGEre9EtlBGP5od2NdGiiucBdKrW96yjuefRrkcHyGojyaOIiMc6iGDZxmEnftuk6pdFCLiy_CpiXug7sB6bZSV_YxKaUdP9-E7kh0wZnWazP6I" target="_blank">
				  	<img class="card-img-top" src="/wp-content/themes/heartland-vineyard/images/tithes-and-offerings.jpg" alt="Support the Mission">
					</a>
				  <div class="card-body">
				    <p class="card-text">Give your tithes and offerings online. You can give a one-time gift, or setup recurring giving.</p>
				    <a href="https://secure.subsplash.com/ui/access/M8277H/ute_H-kP_ft_6ZxUNU2kGGI7Y9S32T1YP0ZM5d1UPGEre9EtlBGP5od2NdGiiucBdKrW96yjuefRrkcHyGojyaOIiMc6iGDZxmEnftuk6pdFCLiy_CpiXug7sB6bZSV_YxKaUdP9-E7kh0wZnWazP6I" class="btn btn-lg btn-outline-light btn-block" target="_blank">Give Online</a>
				  </div>
				</div>
			</div>

			<div class="col">
				<div class="card card-dark">
					<a href="https://secure.subsplash.com/ui/access/M8277H/ute_kONEIyg2xDlKYFUD5w3AHz2YkIhoEOgcPhL0e31YCbxhH-iwaCzxA7m_dMK0z3CD0TcNkz9RjxGttaWFkAzzHQwwqy6I3LbOYW5g03QwCFluYb1mYN_WMIHVvrrKYcdBIyasjVgtjQftPws2zA" target="_blank">
				  	<img class="card-img-top" src="/wp-content/themes/heartland-vineyard/images/compassion-offering.jpg" alt="Compassion Offering">
					</a>
				  <div class="card-body">
				    <p class="card-text">Every week we take up a special offering that we set aside to bless those in need.</p>
				    <a href="https://secure.subsplash.com/ui/access/M8277H/ute_kONEIyg2xDlKYFUD5w3AHz2YkIhoEOgcPhL0e31YCbxhH-iwaCzxA7m_dMK0z3CD0TcNkz9RjxGttaWFkAzzHQwwqy6I3LbOYW5g03QwCFluYb1mYN_WMIHVvrrKYcdBIyasjVgtjQftPws2zA" class="btn btn-lg btn-outline-light btn-block" target="_blank">Give Online</a>
				  </div>
				</div>
			</div>

		</div>

		<div class="row mt-5 text-center">
			<div class="col-md-6 offset-md-3">
				<p><em>Gifts are tax-deductible, contributions on your annual giving statement can be used for deductions on your tax return.</em></p>
			</div>
		</div>

</div>
</section>


<?php get_footer(); ?>
