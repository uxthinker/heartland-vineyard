<?php
/**
 * Template Name: Story - New
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<header class="page-hero">
	<div class="container">
	<div class="page-hero-content">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<?php echo do_shortcode("[types field='page-hero-intro'][/types]"); ?></div>
	</div>
	</div>
</header>



<div class="wrapper" id="page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row justify-content-center">
			<div class="col-lg-8 col-md-10 page-content">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'loop-templates/content', 'page' ); ?>
				<?php endwhile; // end of the loop. ?>
			</div>
		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- /.wrapper -->

<section class="wrapper pt-0">
	<div class="container">
		<!-- What's the service like? -->
		<div class="row justify-content-center text-center">
			<div class="col-lg-8 col-md-10">
				<h2>What's a Sunday gathering like?</h2>
				<p>Our Sunday gatherings are centered around 3 core things:</p>

			</div>
		</div>

		<div class="row">

			<div class="story-card col-lg-4" style="background-image:url(/wp-content/themes/heartland-vineyard/images/sincere-worship.jpg);">
				<a href="#" data-toggle="modal" data-target="#details-worship" class="story-card--body">
					<h4 class="story-card--title">Sincere, Enthusiastic, and Intimate Worship.</h4>
				</a>
			</div>

			<div class="story-card col-lg-4" style="background-image:url(/wp-content/themes/heartland-vineyard/images/travis-mullen-teaching-min.jpg);background-position: left;">
				<a href="#" data-toggle="modal" data-target="#details-teaching" class="story-card--body">
					<h4 class="story-card--title">Solid Biblical Teaching.</h4>
				</a>
			</div>

			<div class="story-card col-lg-4" style="background-image:url(/wp-content/themes/heartland-vineyard/images/hands-on-spirit-led-ministry-time-min.jpg);">
				<a href="#" data-toggle="modal" data-target="#details-ministry-time" class="story-card--body">
					<h4 class="story-card--title">Hands-on, Spirit-led ministry time.</h4>
				</a>
			</div>



		</div><!-- / What's the service like? -->

	</div>
</section>

<!-- Worship Modal -->
<div class="modal fade" id="details-worship" tabindex="-1" role="dialog" aria-labelledby="details-worship" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">We love to worship.</h5>
				<a href="#" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fa fa-times-circle" aria-hidden="true"></i>
				</a>
			</div>
			<div class="modal-body p-5">
				<p>We want to be filled with passion for Jesus. Our desire is to continually grow in our ability to express our love for God in what we say, what we sing and what we do. We usually spend around 20-25 minutes singing praises to our Savior, Jesus. Here's a sample of what to expect:</p>

				<iframe width="560" height="315" src="https://www.youtube.com/embed/FnVP1_-vxI0?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
			</div>

			<div class="modal-footer">
				<a href="/worship-sets/" class="btn btn-outline-dark">Check out more music</a>
			</div>

		</div>
	</div>
</div><!-- / Worship Modal -->

<!-- Teaching Modal -->
<div class="modal fade" id="details-teaching" tabindex="-1" role="dialog" aria-labelledby="details-teaching" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Biblically faithful teaching.</h5>
				<a href="#" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fa fa-times-circle" aria-hidden="true"></i>
				</a>
			</div>
			<div class="modal-body p-5">
				<p>We'll spend around 20 minutes learning from God's word. We strive to be Biblically faithful in all that we say and do. We want God to govern our lives by the authority of the Word of God, and to be enabled by the presence and power of the Holy Spirit. We hope you walk away encouraged and challenged.</p>

				<iframe width="560" height="315" src="https://www.youtube.com/embed/YzhEwKJQM3M?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
			</div>

			<div class="modal-footer">
				<a href="/messages/" class="btn btn-outline-dark">Check out more messages</a>
			</div>

		</div>
	</div>
</div><!-- / Teaching Modal -->

<!-- Ministry Time Modal -->
<div class="modal fade" id="details-ministry-time" tabindex="-1" role="dialog" aria-labelledby="details-ministry-time" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Hands-on ministry time.</h5>
				<a href="#" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fa fa-times-circle" aria-hidden="true"></i>
				</a>
			</div>
			<div class="modal-body p-5">
				<blockquote>
					I tell you the truth, anyone who has faith in me will do what I have been doing. He will do even greater things than these, because I am going to the Father.”
					<cite>- Jesus in John 14:12</cite>
				</blockquote>

				<p>At the end of the gathering we give people an opportunity to receive prayer. If your body isn't working right or you're struggling in any way, we want to pray for you. We lay hands on each other and pray because we believe what Jesus said in John 14:12. We believe He will break through and perform miracles today, just like He did 2000 years ago. We also believe it because we've seen it first hand. Don't miss out on this sweet time.</p>

			</div>

		</div>
	</div>
</div><!-- / Ministry Time Modal -->

<section class="wrapper">
	<div class="container">
		<!-- Get to know us -->
		<div class="row justify-content-center text-center">
			<div class="col-md-8">
				<h2 class="mb-5">Get to know us a little more.</h2>
			</div>
		</div>

		<div class="row">

			<div class="story-card story-card-large col-lg-6" style="background-image:url('/wp-content/themes/heartland-vineyard/images/josh-and-steph-paxton-min.jpg');background-position:top;">
				<a href="/our-story/" class="story-card--body">
					<h4 class="story-card--title">Our Story <i class="material-icons">keyboard_arrow_right</i></h4>
				</a>
			</div>

			<div class="story-card story-card-large col-lg-6" style="background-image:url('/wp-content/themes/heartland-vineyard/images/what-we-believe.jpg');">
				<a href="/our-beliefs/" class="story-card--body">
					<h4 class="story-card--title">Our Beliefs <i class="material-icons">keyboard_arrow_right</i></h4>
				</a>
			</div>

			<div class="story-card col-lg-6" style="background-image:url('/wp-content/uploads/2017/12/irving-outreach.jpg');">
				<a href="/our-mission/" class="story-card--body">
					<h4 class="story-card--title">Our Mission <i class="material-icons">keyboard_arrow_right</i></h4>
				</a>
			</div>

			<!--<div class="story-card col-lg-6" style="background-image:url('/wp-content/uploads/2017/12/everyone-gets-to-play-e1513459112923.jpg');">
				<a href="/our-team/" class="story-card--body">
					<h4 class="story-card--title">Our team <i class="material-icons">keyboard_arrow_right</i></h4>
				</a>
			</div>-->

		</div>
		<!-- / Get to know us -->
	</div>
</section>

<section class="wrapper section-cta">
	<div class="container">
		<h2>We'd love to see you.</h2>
		<p>
			View more details on our service times below.
		</p>
		<a href="#service-times" class="text-white"><i class="material-icons">arrow_downward</i></a>

	</div>
</section>


<?php get_footer(); ?>
