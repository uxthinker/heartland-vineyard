<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

get_header();
$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<div class="wrapper" id="single-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="page-header">

				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				<p class="entry-subtitle"><?php echo do_shortcode("[types field='subtitle'][/types]"); ?></p>

				<p class="entry-date"><?php if ( function_exists( 'coauthors_posts_links' ) ) {
									coauthors();
							} else {
									the_author();
				}?> | <?php echo get_the_date(); ?></p>



				</div>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-md-8">

				<?php while ( have_posts() ) : the_post(); ?>
					<?php if ( has_post_thumbnail() ) {
					    the_post_thumbnail('large');
					} ?>
					<?php get_template_part( 'loop-templates/content', 'single' ); ?>
						<?php //understrap_post_nav(); ?>
				<?php endwhile; // end of the loop. ?>

			</div><!-- /col-md-7 -->


	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
