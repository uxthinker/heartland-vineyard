<section class="wrapper section-cta">
	<div class="container">
		<h2>Stay in the loop</h2>
		<a href="/email-newsletter/" class="btn btn-lg btn-outline-light">Join our email list <i class="material-icons">keyboard_arrow_right</i></a>
	</div>
</section>
