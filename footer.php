<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

<footer class="footer-main">
	<div class="container">

		<div class="contact-info row">

			<div class="col-md-8 contact-info--service-times" id="service-times">
				<h4 class="mb-1">See you Sunday, <span class="service-date"></span>
				</h4>
				<p class="mb-2">
					<?php
				if(get_option('support_address')){ ?>
					<?php echo get_option('support_address'); ?>
				<?php } ?><br>

					<span>
						<?php
							if(get_option('support_services')){ ?>
								<?php echo get_option('support_services'); ?>
						<?php } ?>
					</span>
					</p>
					<p>
					<a href="https://goo.gl/maps/haBgQLjxhBbMTMJe9" target="_blank" class="btn btn-outline-light">Get directions</a> <span class="mx-2">or</span>
					<a href="/livestream/" class="btn btn-outline-light">Join us online</a>
					</p>

			</div>

			<div class="col-md-4 serve-team-info">
				This week's serve team: <a href="/serve-calendar/"><span class="badge badge-light badge-outline badge-serve-week"></span></a><br>
				<a href="/serve-calendar/" class="arrow-link serve-link">
					View the serve calendar
					<i class="material-icons" style="font-size:1rem;">keyboard_arrow_right</i>
				</a>
			</div>


				<script>
					var todaysDate = moment().format('MM/DD/YYYY');
					var gameDay = moment().startOf('week').format('MM/DD/YYYY');
					var gameDayFriendly = moment(gameDay).format('MMM Do');
					var thisWeeksGatheringDate = moment().startOf('week').add(7, 'days').format('MM/DD/YYYY');
					var thisWeeksGatheringDateFriendly = moment(thisWeeksGatheringDate).format('MMM Do');
					//console.log('Game day:'+ gameDay);
					//console.log('This week:'+ thisWeeksGatheringDate);

					orangeStartDate = moment("01/07/2018");
					orangeWeek = orangeStartDate.recur().every(4).weeks();
					orangeWeeks = orangeWeek.next(6, "L");
					//console.log('These are yellow weeks:'+ yellowWeeks);
					var isTodayOrange = orangeWeek.matches(todaysDate);
					//console.log('Is today orange?' + isTodayOrange);
					var isThisWeekOrange = orangeWeek.matches(thisWeeksGatheringDate);
					//console.log('Is this week orange?' + isThisWeekOrange);

					greenStartDate = moment("01/14/2018");
					greenWeek = greenStartDate.recur().every(4).weeks();
					greenWeeks = greenWeek.next(6, "L");
					//console.log('These are green weeks:'+ greenWeeks);
					var isTodayGreen = greenWeek.matches(todaysDate);
					//console.log('Is it green?' + isTodayGreen);
					var isThisWeekGreen = greenWeek.matches(thisWeeksGatheringDate);

					blueStartDate = moment("01/21/2018");
					blueWeek = blueStartDate.recur().every(4).weeks();
					blueWeeks = blueWeek.next(6, "L");
					//console.log('These are blue weeks:'+ blueWeeks);
					var isTodayBlue = blueWeek.matches(todaysDate);
					//console.log('Is today blue?' + isTodayBlue);
					var isThisWeekBlue = blueWeek.matches(thisWeeksGatheringDate);
					//console.log('Is this week blue?' + isThisWeekBlue);

					purpleStartDate = moment("01/28/2018");
					purpleWeek = purpleStartDate.recur().every(4).weeks();
					purpleWeeks = purpleWeek.next(6, "L");
					//console.log('These are purple weeks:'+ purpleWeeks);
					var isTodayPurple = purpleWeek.matches(todaysDate);
					//console.log('Is today purple?' + isTodayPurple);
					var isThisWeekPurple = purpleWeek.matches(thisWeeksGatheringDate);
					//console.log('Is this week purple?' + isThisWeekPurple);


					jQuery('.todays-date').html(todaysDate);



					// Is today Sunday? If so display today's date
					if (moment().isSame(gameDay, 'd')) {
						jQuery('.service-date').html(gameDayFriendly);

						if ( isTodayPurple === true) {
							jQuery('.badge-serve-week').html('Purple').addClass('badge-purple');
							jQuery('.contact-info--service-times').addClass('serve-week-purple');
							//alert('purple!');
						}
						if ( isTodayOrange === true) {
							jQuery('.badge-serve-week').html('Orange').addClass('badge-orange');
							jQuery('.contact-info--service-times').addClass('serve-week-orange');
							//alert('yellow!');
						}
						if ( isTodayBlue === true) {
							jQuery('.badge-serve-week').html('Blue').addClass('badge-blue');
							jQuery('.contact-info--service-times').addClass('serve-week-blue');
							//alert('blue!');
						}
						if ( isTodayGreen === true) {
							jQuery('.badge-serve-week').html('Green').addClass('badge-green');
							jQuery('.contact-info--service-times').addClass('serve-week-green');
							//alert('green!');
						}


					}

					// Show the date of the upcoming Sunday
					else {
						jQuery('.service-date').html(thisWeeksGatheringDateFriendly);

						if ( isThisWeekPurple === true) {
							jQuery('.badge-serve-week').html('Purple').addClass('badge-purple');
							jQuery('.contact-info--service-times').addClass('serve-week-purple');
							//alert('purple!');
						}
						if ( isThisWeekOrange === true) {
							jQuery('.badge-serve-week').html('Orange').addClass('badge-orange');
							jQuery('.contact-info--service-times').addClass('serve-week-orange');
							//alert('yellow!');
						}
						if ( isThisWeekBlue === true) {
							jQuery('.badge-serve-week').html('Blue').addClass('badge-blue');
							jQuery('.contact-info--service-times').addClass('serve-week-blue');
							//alert('blue!');
						}
						if ( isThisWeekGreen === true) {
							jQuery('.badge-serve-week').html('Green').addClass('badge-green');
							jQuery('.contact-info--service-times').addClass('serve-week-green');
							//alert('green!');
						}

					}

				</script>

		</div>
	</div>

<?php get_sidebar( 'footerfull' ); ?>

<div class="copyright">
	<div class="<?php echo esc_attr( $container ); ?>">

		<div class="row">

			<div class="col-md-6 church-info">
				<p>&copy; <?php echo date("Y"); ?> Heartland Vineyard Church.<br class="d-sm-none"> All Rights Reserved.<br>
					<?php
					if(get_option('support_address')){ ?>
						<?php echo get_option('support_address'); ?> <span class="d-none d-sm-inline">|</span><br class="d-sm-none">
					<?php } ?>

				<?php
					if(get_option('support_phone')){ ?>
						<?php echo get_option('support_phone'); ?>
				<?php } ?>
				</p>
			</div><!--col end -->
			<div class="col-md-6 social-media">
				<a href="https://www.facebook.com/Heartland-Vineyard-Church-179307414639/" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
				<a href="https://twitter.com/hrtlandvineyard/" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
				<a href="https://www.instagram.com/hrtlandvineyard/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
				<a href="https://www.youtube.com/user/HeartlandVineyardCF/" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a>
				<a href="https://vimeo.com/heartlandvineyardchurch" target="_blank"><i class="fa fa-vimeo" aria-hidden="true"></i></a>
			</div>
		</div><!-- row end -->
	</div><!-- container end -->
</div><!-- wrapper end -->
</footer>
</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

<script>
//smooth scrolling for anchor links
jQuery(function() {
  jQuery('a.scroll-link[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        jQuery('html, body').animate({
          scrollTop: target.offset().top - 80 //navbar height + 10px
        }, 1000);
        return false;
      }
    }
  });
});


// Stop video when closing a modal
jQuery(document).ready(function(){
    jQuery('.modal').each(function(){
            var src = jQuery(this).find('iframe').attr('src');

        jQuery(this).on('click', function(){

            jQuery(this).find('iframe').attr('src', '');
            jQuery(this).find('iframe').attr('src', src);

        });
    });
});
</script>

<script>
	// Parse the URL
	function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	// Give the URL parameters variable names
	var source = getParameterByName('utm_source');

	// Set the cookies
	if(Cookies.get('utm_source') == null || Cookies.get('utm_source') == "") {
	Cookies.set('utm_source', source);
	}

	jQuery(document).ready(function(){
		if(Cookies.get('utm_source') == 'app') {
			jQuery('html').addClass('app-view');
		}

	});


</script>

</html>
