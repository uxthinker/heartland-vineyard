<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<header class="page-hero">
	<div class="container">
		<div class="page-hero-content">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>
	</div>
</header>

<section class="wrapper pt-0">
<div class="container pb-5">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<script id="subsplash-embed-9mnj3tx" type="text/javascript"> var target = document.getElementById("subsplash-embed-9mnj3tx"); var script = document.createElement("script"); script.type = "text/javascript"; script.onload = function() { subsplashEmbed( "+kt9v/lb/ca/+9mnj3tx?embed", "https://subsplash.com/", "subsplash-embed-9mnj3tx" ); }; script.src = "https://dashboard.static.subsplash.com/production/web-client/external/embed-1.1.0.js"; target.parentElement.insertBefore(script, target); </script>
		</div>
	</div>
</div>
</section>


<?php get_footer(); ?>
