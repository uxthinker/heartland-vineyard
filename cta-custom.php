<section class="wrapper section-cta">
	<div class="container">
		<h2>He loves you, He loves you, He loves you!</h2>
		<p>Did we mention that <strong>Jesus loves <em>you</em></strong>? Nothing this world has to offer will satisfy you. If you're looking for hope, belonging, and purpose in life&mdash;you'll find it in Jesus. We're so grateful you stopped by. If you have a question or need prayer, please reach out.</p>
		<a href="/connect/" class="btn btn-lg btn-outline-light">Let's Connect <i class="material-icons">keyboard_arrow_right</i></a>
	</div>
</section>
