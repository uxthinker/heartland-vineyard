<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

?>

<article class="story-card story-card-post col-md-4" id="post-<?php the_ID(); ?>" style="background-image:url('<?php echo get_the_post_thumbnail_url( $post->ID, 'large' ); ?>');">

<a href="<?php echo get_post_permalink(); ?>" class="story-card--body">
	<?php the_title( '<h4 class="story-card--title">', '</h4>' ); ?>
	<p class="story-card--meta"><?php echo do_shortcode("[types field='teacher'][/types]"); ?> / <?php echo do_shortcode("[types field='sermon-date' style='text' format='F j, Y'][/types]"); ?></p>
</a>

</article><!-- #post-## -->
