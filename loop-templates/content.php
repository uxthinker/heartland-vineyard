<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

?>

<article class="story-card story-card-post col-md-4" id="post-<?php the_ID(); ?>" style="background-image:url('<?php echo get_the_post_thumbnail_url( $post->ID, 'large' ); ?>');">

<a href="<?php echo get_post_permalink(); ?>" class="story-card--body">
	<?php the_title( '<h4 class="story-card--title">', '</h4>' ); ?>
	<p class="story-card--meta"></p>
</a>

</article><!-- #post-## -->
