<?php
/**
 * The template for displaying all messages.
 *
 * @package understrap
 */

get_header();
$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<div class="wrapper" id="single-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">
			<div class="col-md-5 page-header">

				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				<p class="entry-subtitle"><?php echo do_shortcode("[types field='subtitle'][/types]"); ?></p>
				<p class="entry-date" style="font-weight:bold;text-transform:uppercase;"><?php echo do_shortcode("[types field='event-date' style='text' format='l, M j, Y'][/types]"); ?> / <?php echo do_shortcode("[types field='event-location'][/types]"); ?> / <?php echo do_shortcode("[types field='event-time'][/types]"); ?></p>


			</div>

			<div class="col-md-7">

				<?php while ( have_posts() ) : the_post(); ?>
					<?php if ( has_post_thumbnail() ) {
					    the_post_thumbnail('large');
					} ?>
					<?php get_template_part( 'loop-templates/content', 'single' ); ?>
						<?php //understrap_post_nav(); ?>
				<?php endwhile; // end of the loop. ?>

			</div><!-- /col-md-7 -->


	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php echo do_shortcode("[wpv-post-body view_template='custom-event-cta']"); ?>

<?php get_footer(); ?>
