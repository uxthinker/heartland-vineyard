<?php
/**
 * Template Name: Locations
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<header class="page-hero">
	<div class="container">
	<div class="page-hero-content">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<!--<div class="page-header--intro"><?php //echo do_shortcode("[types field='page-hero-intro'][/types]"); ?></div>-->
	</div>
	</div>
</header>

<div class="" id="page-wrapper">

	<div class="container" id="content" tabindex="-1">

		<div class="row">
			<div class="col-lg-4">
				<div class="page-content--featured-image"><?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?></div>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'loop-templates/content', 'page' ); ?>
				<?php endwhile; // end of the loop. ?>
			</div>
			<div class="col-lg-8">
				<?php echo do_shortcode("[wpv-view name='locations']"); ?>
			</div>

		</div><!-- #primary -->


	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<section class="wrapper section-cta">
	<div class="container">
		<h2>He loves you, He loves you, He loves you!</h2>
		<p>Did we mention that <strong>Jesus loves <em>you</em></strong>? Nothing this world has to offer will satisfy you. If you're looking for hope, belonging, and purpose in life&mdash;you'll find it in Jesus. We're so greatful you stopped by. If you have a question or need prayer, please reach out.</p>
		<a href="#" class="btn btn-lg btn-outline-light">Let's Connect</a>
	</div>
</section>

<div class="container-fluid">
<div class="row">
	<div class="story-card story-card-new col-lg-3">
		<a href="#" class="story-card--body">
			<h4 class="story-card--title">Hi, I'm <span>new</span></h4>
		</a>
	</div>

	<div class="story-card story-card-belong col-lg-3">
		<a href="#" class="story-card--body">
			<h4 class="story-card--title">I want to <span>belong</span></h4>
		</a>
	</div>

	<div class="story-card story-card-hope col-lg-3">
		<a href="#" class="story-card--body">
			<h4 class="story-card--title">I'm seeking <span>hope</span></h4>
		</a>
	</div>

	<div class="story-card story-card-impact col-lg-3">
		<a href="#" class="story-card--body">
			<h4 class="story-card--title">I need a <span>purpose</span></h4>
		</a>
	</div>

</div>
</div>

<?php get_footer(); ?>
