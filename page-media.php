<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<header class="page-hero">
	<div class="container">
		<div class="page-hero-content">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>
	</div>
</header>

<section class="wrapper section-teal">
<div class="container">
	<!--<div class="row mb-4 text-center text-md-left">
		<div class="col">
			<h2>Messages</h2>
		</div>
	</div>-->
	<div class="row mb-2">
			<?php echo do_shortcode("[wpv-view name='latest-sermon-media']"); ?>
	</div>
	<div class="row">
		<div class="col text-center">
			<a href="/sermon-series/" class="arrow-link">View more series<i class="material-icons" style="font-size:1rem;">keyboard_arrow_right</i></a>
		</div>
	</div>
</div>
</section>

<section class="wrapper">
	<div class="container pb-5">
		<div class="row mb-4 text-center text-md-left">
			<div class="col">
				<h2>Recent Messages</h2>
			</div>
		</div>
		<div class="row">
			<?php echo do_shortcode("[wpv-view name='newer-sermons-media']"); ?>
		</div>
		<div class="row">
			<div class="col text-center"><a href="/category/messages/" class="arrow-link text-dark">View all messages<i class="material-icons" style="font-size:1rem;">keyboard_arrow_right</i></a></div>
		</div>
	</div>
</section>

<section class="wrapper section-dark">
<div class="container pb-5">
	<div class="row mb-4 text-center text-md-left">
		<div class="col">
			<h2>Recent Worship Sets</h2>
		</div>
	</div>
	<?php echo do_shortcode("[wpv-view name='latest-worship-sets']"); ?>
	<div class="row">
		<div class="col text-center">
			<a href="/worship/" class="arrow-link">View all worship sets<i class="material-icons" style="font-size:1rem;">keyboard_arrow_right</i></a>
		</div>
	</div>
</div>
</section>

<?php include 'cta-lets-connect.php' ?>

<?php get_footer(); ?>
