<?php
/**
 * Template Name: Story - Purpose
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<header class="row split-hero align-items-center">
	<div class="col-lg-6 col-md-5 col-12 split-hero--image" style="background-image:url('<?php echo get_the_post_thumbnail_url( $post->ID, 'large' ); ?>');">

	</div>
	<div class="col-lg-6 col-md-7 col-12 split-hero--header">

		<div class="row justify-content-center">
	    <div class="col-md-8 col-10">

				<?php
				    foreach((get_the_category()) as $category){
								echo "<span class='badge badge-warning'>".$category->name."</span>";
				        }
				    ?>

	      <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				<div class="page-header--intro">
					<?php echo do_shortcode("[types field='page-hero-intro'][/types]"); ?>
				</div>

	    </div>
  	</div>

	</div>
</header>

<div class="wrapper" id="page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row justify-content-center">
			<div class="col-lg-8 col-md-10 page-content">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'loop-templates/content', 'page' ); ?>
				<?php endwhile; // end of the loop. ?>
			</div>

		</div><!-- #primary -->


	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<section class="wrapper section-cta">
	<div class="container">
		<h2>Are you ready to discover your purpose?</h2>
		<p>God has big plans for you. Let's figure it out together.</p>
		<a href="/community/" class="btn btn-lg btn-outline-light">Find Community <i class="material-icons">keyboard_arrow_right</i></a>
	</div>
</section>

<?php get_footer(); ?>
