<?php
/**
 * The template for the Sermons category.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();
?>

<?php
$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<header class="page-hero">
	<div class="container">
	<div class="page-hero-content">
		<p class="mb-1">SERMON SERIES</p>
		<?php single_term_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</div>
	</div>
</header>

<div class="wrapper" id="archive-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

				<?php if ( have_posts() ) : ?>

					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'loop-templates/content-messages', get_post_format() );
						?>

					<?php endwhile; ?>

				<?php else : ?>

					<?php get_template_part( 'loop-templates/content-messages', 'none' ); ?>

				<?php endif; ?>

				<div class="row mt-5">
					<div class="col-md-8">
						<!-- The pagination component -->
						<?php understrap_pagination(); ?>
					</div>
				</div>

		</div><!-- row -->

	</div> <!-- .row -->





</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
