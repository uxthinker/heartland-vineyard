<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<header class="page-hero">
	<div class="container">
	<div class="page-hero-content">
		<h1 class="entry-title">Message Archives</h1>
		<div class="row justify-content-md-center mb-5">
			<div class="col-md-5">
				<div class="btn-group d-flex" role="group" aria-label="Basic example">
				  <a href="/sermon/" class="btn w-100 btn-outline-light">By Date</a>
				  <a href="/sermon-series/" class="btn w-100 btn-outline-light active">By Series</a>
				</div>
			</div>
		</div>
	</div>
	</div>
</header>

<section class="wrapper">
<div class="container">
	<?php echo do_shortcode("[wpv-view name='featured-series']"); ?>
</div>
</section>

<?php include 'cta-lets-connect.php' ?>

<?php get_footer(); ?>
