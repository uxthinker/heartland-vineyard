<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<section class="wrapper section-dark" id="page-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h1>We're sorry, that page doesn't exist.</h1>
				<p>What are you looking for?</p>
			</div>
		</div>
		<div class="row">
			<div class="story-card story-card-new col-lg-6">
				<a href="/what-to-expect/" class="story-card--body">
					<h4 class="story-card--title">Hi, I'm <span>new</span></h4>
				</a>
			</div>

			<div class="story-card story-card-belong col-lg-6">
				<a href="/i-want-to-belong/" class="story-card--body">
					<h4 class="story-card--title">I want to <span>belong</span></h4>
				</a>
			</div>

			<div class="story-card story-card-hope col-lg-6">
				<a href="/im-desperate-need-hope/" class="story-card--body">
					<h4 class="story-card--title">I'm looking for <span>hope</span></h4>
				</a>
			</div>

			<div class="story-card story-card-impact col-lg-6">
				<a href="/i-need-a-purpose/" class="story-card--body">
					<h4 class="story-card--title">I need a <span>purpose</span></h4>
				</a>
			</div>

		</div>
	</div>
</section>




<?php include 'cta-lets-connect.php' ?>

<?php get_footer(); ?>
