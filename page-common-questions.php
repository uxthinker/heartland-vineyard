<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<header class="page-hero">
	<div class="container">
	<div class="page-hero-content">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</div>
	</div>
</header>

<div class="wrapper" id="page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row justify-content-center">
			<div class="col-lg-8 col-md-10 page-content">
				<div class="page-content--featured-image"><?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?></div>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'loop-templates/content', 'page' ); ?>
				<?php endwhile; // end of the loop. ?>
			</div>

		</div><!-- #primary -->


	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<section class="wrapper section-cta">
	<div class="container">
		<h2>Still have questions?</h2>
		<p>If you can't find the answer to your question, let us know!</p>
		<a href="/connect/" class="btn btn-lg btn-outline-light">Let's Connect <i class="material-icons">keyboard_arrow_right</i></a>
	</div>
</section>


<?php get_footer(); ?>
