<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<header class="page-hero">
	<div class="container">
	<div class="page-hero-content">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<?php echo do_shortcode("[types field='page-hero-intro'][/types]"); ?></div>
	</div>
	</div>
</header>

<div class="wrapper" id="page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row justify-content-center">
			<div class="col-lg-8 col-md-10 page-content">
				<div class="page-content--featured-image"><?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?></div>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'loop-templates/content', 'page' ); ?>



				<?php endwhile; // end of the loop. ?>
			</div>

		</div><!-- #primary -->


	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<section class="wrapper section-dark">
<div class="container">

	<div class="row justify-content-md-center mb-5">
		<div class="col-md-5 text-center">
			<h5>We are currently operating in:</h5>
			<h1>Phase <span class="badge badge-dark phase-high" style="position:relative; bottom:3px;">1</span></h1>

		</div>
	</div>

	<div class="row justify-content-md-center">
		<div class="col">

			<table class="table table-bordered table-dark table-reopening table-responsive">
				<thead>
					<tr class="text-center">
						<th scope="col" style="width:20%" class=""></th>
						<th scope="col" style="width:20%" class="phase-1 phase-high">Phase 1</th>
						<th scope="col" style="width:20%" class="phase-2 phase-moderate">Phase 2</th>
						<th scope="col" style="width:20%" class="phase-3 text-dark phase-elevated">Phase 3</th>
						<th scope="col" style="width:20%" class="phase-4 phase-low">Phase 4</th>
					</tr>
				</thead>
				<tbody>
					<tr class="text-center">
						<th class="text-left">Level of Community Transmission
							<p class="mb-0 text-muted text-small">(Black Hawk County status per the IDPH)</p>
						</th>
						<td class="phase-1"><h6>High/Substantial</h6></td>
						<td class="phase-2"><h6>Moderate</h6></td>
						<td class="phase-3"><h6>Minimal/Elevated</h6></td>
						<td class="phase-4"><h6>Low/None</h6></td>
					</tr>
					<tr class="text-center row-border-bottom">
						<th class="text-left">HVC Response</th>
						<td class="phase-1">50% capacity<br> <span class="text-muted">Socially Distanced</span></td>
						<td class="phase-2">50% capacity<br> <span class="text-muted">Socially Distanced</span></td>
						<td class="phase-3">50-75% capacity<br> <span class="text-muted">Socially Distanced</span></td>
						<td class="phase-4">Full capacity<br> <span class="text-muted">Regular gatherings</span></td>
					</tr>
					<tr>
						<th scope="col">Weekend Services</th>
						<td class="phase-1">
							<div class="text-center mb-3">
								<h6>In-Person + Livestream</h6>
								<p class="mb-0"><span class="text-muted">10:00a.m.</span></p>
							</div>
							<ul>
								<li>Weekend service in the building with 50% capacity</li>
								<li>We encourage vulnerable populations to stay home during this phase.</li>
								<li>Masks required</li>
								<!--<li>At 10:45: Masks upon entry to our church building (unless under the age of 5) and while in common spaces. Once seated and appropriately spaced, masks can be removed, if desired.</li>
								<li>VC Kids open at 10:45 gathering. Masks required due to inability to social distance.</li>-->
							</ul>
						</td>
						<td class="phase-2">
							<div class="text-center mb-3">
								<!--<p><span class="badge badge-light" style="font-size:.9rem;">Starting June 14th</span></p>-->
								<h6>In-Person + Livestream</h6>
								<p class="mb-0"><span class="text-muted">9:00 & 10:45a.m.</span></p>
							</div>
							<ul>
								<li>Weekend services in the building with 50% capacity.</li>
								<li>We encourage vulnerable populations to stay home during this phase.</li>
								<li>Masks required at 9:00 gathering.</li>
								<li>Masks welcomed at our 10:45 gathering</li>
								<li>VC Kids open at 10:45 gathering. Masks required do to inability to social distance.</li>
							</ul>
						</td>
						<td class="phase-3">
							<div class="text-center mb-3">
								<h6>In-Person + Livestream</h6>
								<p class="mb-0"><span class="text-muted">9:00 & 10:45a.m.</span></p>
							</div>
							<ul>
								<li>Weekend services in the building with continued capacity at 50-75%</li>
								<li>Masks welcomed at both gatherings</li>
								<li>VC Kids open at 9:00 and 10:45 gatherings</li>
							</ul>
						</td>
						<td class="phase-4">
							<div class="text-center mb-3">
								<h6>In-Person + Livestream</h6>
								<p class="mb-0"><span class="text-muted">9:00 & 10:45a.m.</span></p>
							</div>
							<ul>
								<li>Weekend services open with full capacity</li>
								<li>Masks welcomed at both gatherings</li>
								<li>VC Kids open at 9:00 and 10:45 gatherings</li>
							</ul>
						</td>
					</tr>
					<!--<tr>
						<th scope="col">Community Life
							<p class="mb-0 text-muted text-small">Youth, kidz@nite, Freedom Flow, and Small Groups</p>
						</th>
						<td class="phase-1">
							<p class="mb-0 text-muted">Join via Zoom</p>
						</td>
						<td class="phase-2"><p class="mb-0 text-muted">May gather in homes or other locations (other than the church building)</p></td>
						<td class="phase-3"><p class="mb-0 text-muted">Gather in homes or in the building with new guidelines</p></td>
						<td class="phase-4"><p class="mb-0 text-muted">Gather in homes or in the building with new guidelines</p></td>
					</tr>-->
				</tbody>
			</table>

		</div>
	<div>

</div>
</section>



<?php include 'cta-lets-connect.php' ?>

<?php get_footer(); ?>
