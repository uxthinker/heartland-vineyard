<?php
/**
 * Template Name: Form pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<header class="page-hero">
	<div class="container">
	<div class="page-hero-content">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<!--<div class="page-header--intro"><?php //echo do_shortcode("[types field='page-hero-intro'][/types]"); ?></div>-->
	</div>
	</div>
</header>

<section class="wrapper section-dark" id="page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row justify-content-center">
			<div class="col-lg-8 col-md-10 page-content">
				<div class="page-content--featured-image"><?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?></div>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'loop-templates/content', 'page' ); ?>
				<?php endwhile; // end of the loop. ?>
			</div>

		</div><!-- #primary -->


	</div><!-- .row -->

</div><!-- Container end -->

</section><!-- Wrapper end -->

<?php get_footer(); ?>
