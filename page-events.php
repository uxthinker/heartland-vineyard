<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<header class="page-hero">
	<div class="container">
		<div class="page-hero-content">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>
	</div>
</header>

<section class="wrapper section-dark pt-0">
<div class="container pb-5">
	<?php echo do_shortcode("[wpv-view name='featured-news-posts']"); ?>
</div>
</section>

<!--<section class="wrapper">
	<div class="container">
		<?php //echo do_shortcode("[ccbpress_event_calendar]"); ?>
	</div>
</section>-->

<section class="wrapper section-cta">
	<div class="container">
		<h2>Stay in the loop</h2>
		<p>If you want to stay informed about what is going on, join our email list!</p>
		<a href="/email-newsletter/" class="btn btn-lg btn-outline-light">Join our email list</a>
	</div>
</section>

<?php get_footer(); ?>
