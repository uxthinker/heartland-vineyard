<?php
/**
 * The template for displaying all messages.
 *
 * @package understrap
 */

get_header();
$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<div class="wrapper" id="single-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">
			<div class="col-md-5 page-header">

				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				<p class="entry-subtitle"><?php echo do_shortcode("[types field='subtitle'][/types]"); ?></p>
				<p class="entry-date" style="font-weight:bold;text-transform:uppercase;"><?php echo do_shortcode("[types field='teacher'][/types]"); ?> / <?php echo do_shortcode("[types field='sermon-date' style='text' format='F j, Y'][/types]"); ?></p>

			</div>

			<div class="col-md-7">

				<?php //echo do_shortcode("[wpv-post-body view_template='media-tabs']"); ?>

				<div class="tab-content" id="media-panel">
				  <div class="tab-pane fade show active" id="video" role="tabpanel" aria-labelledby="video-tab">
						<?php while ( have_posts() ) : the_post(); ?>

							<?php get_template_part( 'loop-templates/content', 'single' ); ?>
								<?php //understrap_post_nav(); ?>
						<?php endwhile; // end of the loop. ?>
					</div>
				  <!--<div class="tab-pane fade" id="audio" role="tabpanel" aria-labelledby="audio-tab">
						<?php //echo do_shortcode("[wpv-post-body view_template='sermon-audio-embed']"); ?>
					</div>-->
				</div>

			</div><!-- /col-md-7 -->

	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
