<?php
/**
 * Template Name: Homepage 2
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<section class="hero">
	<div class="container">
		<p>We're on a mission to equip an army of fully devoted disciples to</p>
		<h1>Experience and Express the Compassion of Jesus.</h1>

		<a href="#" class="btn btn-lg btn-outline-light">Are you in?</a>
	</div>
</section>

<div class="wrapper section-white" id="page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row no-gutters align-items-center">

			<div class="col-lg-4">

				<?php while ( have_posts() ) : the_post(); ?>
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					<?php get_template_part( 'loop-templates/content', 'page' ); ?>



				<?php endwhile; // end of the loop. ?>

			</div><!-- main col -->

			<div class="col-lg-7 ml-auto">
				<div class="row">

					<div class="story-card story-card-new col-lg-6">
					  <a href="#" class="story-card--body">
							<h4 class="story-card--title">Hi, I'm <span>new</span></h4>
					  </a>
					</div>

					<div class="story-card story-card-belong col-lg-6">
					  <a href="#" class="story-card--body">
							<h4 class="story-card--title">I want to <span>belong</span></h4>
					  </a>
					</div>

					<div class="story-card story-card-hope col-lg-6">
					  <a href="#" class="story-card--body">
							<h4 class="story-card--title">I'm seeking <span>hope</span></h4>
					  </a>
					</div>

					<div class="story-card story-card-impact col-lg-6">
					  <a href="#" class="story-card--body">
							<h4 class="story-card--title">I need a <span>purpose</span></h4>
					  </a>
					</div>

				</div>

				<p class="more-questions">I have a different <a href="#" class="btn btn-outline-dark">question</a></p>

			</div>

		</div><!-- #primary -->


	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<section class="wrapper section-image section-image-mission" style="display:none;">
	<span class="badge badge-warning">Our Mission</span>
	<div class="container">
		<h2>We gather to <a href="/our-mission" class="btn btn-outline-light">scatter</a>.</h2>
		<p>We love to gather together on Sundays, but our purpose doesn't end there. We're not a social club that just talks about Jesus. We are called to action. When we walk out the door, we're on a mission to express the compassion and truth of Jesus to those around us. We want to see Jesus break through the darkness&mdash;to change people's lives.</p>
	</div>
</section>

<section class="wrapper section-image section-image-message">
	<span class="badge badge-warning">Latest Message</span>
	<div class="container">
		<h2><a href="#">Unchangeable</a></h2>
		<p><a href="#" class="btn btn-outline-light"><i class="fa fa-play" aria-hidden="true"></i> Watch</a> or <a href="#" class="btn btn-outline-light"><i class="fa fa-headphones" aria-hidden="true"></i> Listen</a></p>
	</div>
	<p class="link-past-messages">Check out <a href="#" class="btn btn-outline-light">past messages</a></p>
</section>

<section class="wrapper section-weekly-events">
	<div class="container text-center">
		<h2>Weekly Events</h2>
		<div class="row">
			<div class="col-lg-6">
				<div class="row" style="height:100%;">
					<div class="story-card story-card-large story-card-sunday-gathering col-lg-12">
						<span class="badge badge-secondary">Sun</span>
						<a href="/events-calendar/event/?ccbpress_event_id=1252" class="story-card--body">
							<h4 class="story-card--title">Sunday Gathering</h4>
						</a>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="row">
					<div class="story-card story-card-cr col-lg-6">
						<span class="badge badge-secondary">Tue</span>
						<a href="/events-calendar/event/?ccbpress_event_id=1130" class="story-card--body">
							<h4 class="story-card--title">Celebrate Recovery</h4>
						</a>
					</div>
					<div class="story-card story-card-kids col-lg-6">
						<span class="badge badge-secondary">Wed</span>
						<a href="#" class="story-card--body">
							<h4 class="story-card--title">Kids@Nite</h4>
						</a>
					</div>
					<div class="story-card story-card-students col-lg-6">
						<span class="badge badge-secondary">Wed</span>
						<a href="#" class="story-card--body">
							<h4 class="story-card--title">VC Students</h4>
						</a>
					</div>
					<div class="story-card story-card-street-ministry col-lg-6">
						<span class="badge badge-secondary">Thu</span>
						<a href="/events-calendar/event/?ccbpress_event_id=1325" class="story-card--body">
							<h4 class="story-card--title">Street Ministry</h4>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="row">
					<div class="story-card story-card-large story-card-small-groups col-lg-12">
						<span class="badge badge-secondary">Daily</span>
						<a href="#" class="story-card--body">
							<h4 class="story-card--title">Small Groups</h4>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="wrapper section-cta">
	<div class="container">
		<h2>He loves you, He loves you, He loves you!</h2>
		<p>Did we mention that <strong>Jesus loves <em>you</em></strong>? Nothing this world has to offer will satisfy you. If you're looking for hope, belonging, and purpose in life&mdash;you'll find it in Jesus. We're so greatful you stopped by. If you have a question or need prayer, please reach out.</p>
		<a href="#" class="btn btn-lg btn-outline-light">Let's Connect</a>
	</div>
</section>

<?php get_footer(); ?>
