<?php
/**
 * Template Name: Homepage
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<section class="hero">

	<div class="container">

		<h1>Growing<br> Together</h1>
		<p class="subheading">For the sake of our communities!</p>


	</div>
	<a href="#page-wrapper" class="btn btn-lg btn-outline-light scroll-link"><i class="material-icons">expand_more</i></a>
</section>

<div class="wrapper" id="page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row no-gutters align-items-center">

			<div class="col-lg-6">

				<?php while ( have_posts() ) : the_post(); ?>
					<h1 class="h1-home">Welcome <span>to the Vineyard</span></h1>
					<?php get_template_part( 'loop-templates/content', 'page' ); ?>



				<?php endwhile; // end of the loop. ?>

			</div><!-- main col -->

			<div class="col-lg-5 ml-auto">
				<div class="row">

						<img src="/wp-content/themes/heartland-vineyard/images/photo-montage.jpg">

				</div>


			</div>

		</div><!-- #primary -->


	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<section class="wrapper section-image section-image-mission">
	<div class="container">
		<h2>We are<br> the Church</h2>
		<div class="row">
			<div class="col-md">
				<h3><span class="circle">1</span><span class="mission">Belong</span></h3>
				<p class="mission-desc">Live life in community.</p>
			</div>
			<div class="col-md">
				<h3><span class="circle">2</span><span class="mission">Grow</span></h3>
				<p class="mission-desc">Spend time in God’s presence. (pray, listen, worship, study)</p>
			</div>
			<div class="col-md">
				<h3><span class="circle">3</span><span class="mission">Go</span></h3>
				<p class="mission-desc">Respond to the voice of Holy Spirit.</p>
			</div>
		</div>
	</div>
</section>

<section class="wrapper section-weekly-events">
	<div class="container text-center">

		<div class="row justify-content-center">
			<div class="col-8">
				<h2 class="mb-4">Find Community</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-6">
				<div class="row" style="height:100%;">
					<div class="story-card story-card story-card-large story-card-sunday-gathering col-lg-12">
						<a href="/what-to-expect/" class="story-card--body">
							<h4 class="story-card--title mb-0">Sunday<br> Gatherings</h4>
							<p>
								<?php
									if(get_option('support_services')){ ?>
										<?php echo get_option('support_services'); ?>
								<?php } ?>
							</p>
						</a>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="row" style="height:100%;">
					<div class="story-card story-card-large story-card-small-groups col">
						<a href="/ministry/small-groups/" class="story-card--body">
							<h4 class="story-card--title mb-0">Community<br> Groups</h4>
							<p>Daily</p>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<!--<div class="col-lg-4">
				<div class="row" style="height:100%;">
					<div class="story-card story-card-large story-card-equip-breakfasts col">
						<a href="/ministry/equip-breakfast/" class="story-card--body">
							<h4 class="story-card--title mb-0">Equip<br> Breakfasts</h4>
							<p>2nd Sat of each month at 9:00am</p>
						</a>
					</div>
				</div>
			</div>-->
			<div class="col-lg-6">
				<div class="row" style="height:100%;">
					<div class="story-card story-card story-card-large story-card-students col-lg-12">
						<a href="/ministry/youth/" class="story-card--body">
							<h4 class="story-card--title mb-0 mt-4">Youth</h4>
							<p>Wed from 6:30-8:00pm</p>
						</a>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="row" style="height:100%;">
					<div class="story-card story-card-large story-card-kids col">
						<a href="/ministry/kidznite/" class="story-card--body">
							<h4 class="story-card--title mb-0 mt-4">Kidz@nite</h4>
							<p>Wed from 6:30-8:00pm</p>
						</a>
					</div>
				</div>
			</div>
		</div>


	</div><!-- /container -->
</section>

<section class="wrapper section-image section-image-boundless">
	<div class="container">
		<div class="row justify-content-center text-center">
			<div class="col-md-8">
				<h2>Declaring & Delivering <span>God’s Boundless love</span></h2>
				<p>to a broken world</p>
			</div>
	</div>
	</div>
</section>

<?php include 'cta-lets-connect.php' ?>

<?php get_footer(); ?>
