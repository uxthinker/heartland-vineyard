<?php
/**
* Template Name: Livestream
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<header class="page-hero">
	<div class="container">
	<div class="page-hero-content">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<?php echo do_shortcode("[types field='page-hero-intro'][/types]"); ?></div>
	</div>
	</div>
</header>

<section class="wrapper section-dark pt-3">
<div class="container">
	<p class="text-center">Join us online! Sundays,
		<?php
		if(get_option('support_services')){ ?>
			<?php echo get_option('support_services'); ?>
	<?php } ?> CST.</p>
  <?php while ( have_posts() ) : the_post(); ?>
    <?php get_template_part( 'loop-templates/content', 'page' ); ?>
  <?php endwhile; // end of the loop. ?>
</div>
</section>

<section class="wrapper section-dark pt-0">
	<div class="container">
		<div class="row text-center">
			<div class="col">
				<h2><span>3 Ways</span> to give</h2>
			</div>
		</div>
	</div>
<div class="container pb-5">
	<div class="row text-center">
	  <div class="col-md-4">
			<span class="material-icons give-icons">mail_outline</span>
			<h3>Mail</h3>
			<p>Mail your gift to:<br>
				3211 Titan Trail<br>
				Waterloo, IA 50701
			</p>
		</div>
		<div class="col-md-4">
			<span class="material-icons give-icons">phonelink</span>
			<h3>Online</h3>
			<p><a href="/give/" class="btn btn-outline-light" target="_blank">Give online</a></p>
		</div>
		<div class="col-md-4">
			<span class="material-icons give-icons">textsms</span>
			<h3>Text</h3>
			<p>Text any amount<br> to "Heartland Vineyard"<br> at 206-859-9405.</p>
		</div>
	</div>
</div>
</section>

<section class="wrapper section-cta">
	<div class="container">
		<h2>Let's connect.</h2>
		<p>We're glad you could join us today. If you have a prayer request or you would like to share a story of God working in your life please let us know! We would love the opportunity to pray for you!</p>
		<a href="/connect-card/" class="btn btn-lg btn-outline-light">Fill out a Connect Card <i class="material-icons">keyboard_arrow_right</i></a>
	</div>
</section>



<?php get_footer(); ?>
