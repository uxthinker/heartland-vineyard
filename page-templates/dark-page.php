<?php
/**
 * Template Name: Dark page
 *
 * This is a custom page template.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<header class="page-hero">
	<div class="container">
	<div class="page-hero-content">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
    <?php echo do_shortcode("[types field='page-hero-intro'][/types]"); ?></div>
	</div>
	</div>
</header>

<section class="wrapper section-dark pt-0">
<div class="container pb-5">
  <?php while ( have_posts() ) : the_post(); ?>
    <?php get_template_part( 'loop-templates/content', 'page' ); ?>
  <?php endwhile; // end of the loop. ?>
</div>
</section>

<?php include 'cta-lets-connect.php' ?>

<?php get_footer(); ?>
