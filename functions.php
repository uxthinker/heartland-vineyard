<?php
function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

  add_filter( 'get_the_archive_title', function ($title) {

      if ( is_category() ) {

              $title = single_cat_title( '', false );

          } elseif ( is_tag() ) {

              $title = single_tag_title( '', false );

          } elseif ( is_author() ) {

              $title = '<span class="vcard">' . get_the_author() . '</span>' ;

          }

      return $title;

  });

  add_image_size( 'ministry-image', 568, 456, TRUE );

  function jptweak_remove_share() {
    remove_filter( 'the_content', 'sharing_display', 19 );
    remove_filter( 'the_excerpt', 'sharing_display', 19 );
    if ( class_exists( 'Jetpack_Likes' ) ) {
        remove_filter( 'the_content', array( Jetpack_Likes::init(), 'post_likes' ), 30, 1 );
    }
}

add_action( 'loop_start', 'jptweak_remove_share' );

	// Get the theme data
	$the_theme = wp_get_theme();

    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'popper-scripts', get_stylesheet_directory_uri() . '/js/popper.min.js', array(), false);
    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
}

/*===================================================================================
* Add global options
* =================================================================================*/

/**
 *
 * The page content surrounding the settings fields. Usually you use this to instruct non-techy people what to do.
 *
 */
function theme_settings_page(){
	?>
	<div class="wrap">
		<h1>Contact Info</h1>
		<p>This information is used around the website, so changing these here will update them across the website.</p>
		<form method="post" action="options.php">
			<?php
			settings_fields("section");
			do_settings_sections("theme-options");
			submit_button();
			?>
		</form>
	</div>

	<?php }
/**
 *
 * Next comes the settings fields to display. Use anything from inputs and textareas, to checkboxes multi-selects.
 *
 */
// Phone
function display_support_phone_element(){ ?>

	<input type="tel" name="support_phone" placeholder="Enter phone number" value="<?php echo get_option('support_phone'); ?>" size="35">

<?php }

// Address
function display_support_address_element(){ ?>

	<input type="tel" name="support_address" placeholder="Enter address" value="<?php echo get_option('support_address'); ?>" size="50">

<?php }
// Email
function display_support_services_element(){ ?>

	<input type="text" name="support_services" placeholder="Enter service times" value="<?php echo get_option('support_services'); ?>" size="50">

<?php }
/**
 *
 * Here you tell WP what to enqueue into the <form> area. You need:
 *
 * 1. add_settings_section
 * 2. add_settings_field
 * 3. register_setting
 *
 */
function display_custom_info_fields(){

	add_settings_section("section", "Church Information", null, "theme-options");
	add_settings_field("support_phone", "Phone No.", "display_support_phone_element", "theme-options", "section");
  add_settings_field("support_address", "Address", "display_support_address_element", "theme-options", "section");
	add_settings_field("support_services", "Sunday Gathering Times", "display_support_services_element", "theme-options", "section");
	register_setting("section", "support_phone");
  register_setting("section", "support_address");
	register_setting("section", "support_services");

}
add_action("admin_init", "display_custom_info_fields");
/**
 *
 * Tie it all together by adding the settings page to wherever you like. For this example it will appear
 * in Settings > Contact Info
 *
 */
function add_custom_info_menu_item(){

	add_options_page("Contact Info", "Contact Info", "manage_options", "contact-info", "theme_settings_page");

}
add_action("admin_menu", "add_custom_info_menu_item");
