<div class="story-card story-card-new col-lg-6">
  <a href="/what-to-expect/" class="story-card--body">
    <h4 class="story-card--title">Hi, I'm <span>new</span></h4>
  </a>
</div>

<div class="story-card story-card-belong col-lg-6">
  <a href="/i-want-to-belong/" class="story-card--body">
    <h4 class="story-card--title">I want to <span>belong</span></h4>
  </a>
</div>

<div class="story-card story-card-hope col-lg-6">
  <a href="/im-desperate-need-hope/" class="story-card--body">
    <h4 class="story-card--title">I'm seeking <span>hope</span></h4>
  </a>
</div>

<div class="story-card story-card-impact col-lg-6">
  <a href="/i-need-a-purpose/" class="story-card--body">
    <h4 class="story-card--title">I need a <span>purpose</span></h4>
  </a>
</div>
