<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

get_header();
$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<header class="page-hero">
	<div class="container">
	<div class="page-hero-content">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<div class="page-header--intro"><?php echo do_shortcode("[types field='day-of-the-week'][/types]"); ?>  <?php echo do_shortcode("[types field='subheading'][/types]"); ?></div>
	</div>
	</div>
</header>

<div class="wrapper" id="single-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row justify-content-center">
			<div class="col-lg-8 col-md-10 ">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'loop-templates/content', 'single' ); ?>

				<?php endwhile; // end of the loop. ?>

			</div><!-- /col-md-7 -->


	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php echo do_shortcode("[wpv-post-body view_template='custom-cta']"); ?>

<?php get_footer(); ?>
