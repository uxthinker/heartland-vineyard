<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<header class="page-hero">
	<div class="container">
		<div class="page-hero-content">
			<h1 class="entry-title">Media</h1>
			<div class="row justify-content-md-center mb-5">
				<div class="col-md-5">
					<div class="btn-group d-flex" role="group" aria-label="Basic example">
					  <a href="/messages/" class="btn w-100 btn-outline-light">Messages</a>
					  <a href="/worship-sets/" class="btn w-100 btn-outline-light active">Worship Sets</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<div class="wrapper" id="page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row justify-content-center">
			<div class="col-md-12 page-content">
				<div class="page-content--featured-image"><?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?></div>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'loop-templates/content', 'page' ); ?>
				<?php endwhile; // end of the loop. ?>
			</div>


		</div><!-- #primary -->


	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
