<section class="wrapper section-weekly-events section-dark">
	<div class="container text-center">
		<h2>Find Community</h2>
		<div class="row">
			<div class="col-lg-6">
				<div class="row" style="height:100%;">
					<div class="story-card story-card story-card-sunday-gathering col-lg-12">
						<span class="badge badge-secondary">Sun</span>
						<a href="/what-to-expect/" class="story-card--body">
							<h4 class="story-card--title">Sunday Gatherings</h4>
							<p>
								<?php
									if(get_option('support_services')){ ?>
										<?php echo get_option('support_services'); ?>
								<?php } ?>
							</p>
						</a>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="row">

					<div class="story-card story-card-ff col-lg-6">
						<span class="badge badge-secondary">Tue</span>
						<a href="/ministry/freedom-flow/" class="story-card--body">
							<h4 class="story-card--title">Freedom Flow</h4>
						</a>
					</div>

					<div class="story-card story-card-kids col-lg-6">
						<span class="badge badge-secondary">Wed</span>
						<a href="/ministry/kidznite/" class="story-card--body">
							<h4 class="story-card--title">Kidz@Nite</h4>
						</a>
					</div>

				</div>

				<div class="row">


					<div class="story-card story-card-students col-lg-6">
						<span class="badge badge-secondary">Wed</span>
						<a href="/ministry/youth/" class="story-card--body">
							<h4 class="story-card--title">VC Youth</h4>
						</a>
					</div>
					<div class="story-card story-card-3-12 col-lg-6">
						<span class="badge badge-secondary">Thu</span>
						<a href="/ministry/312-transformation/" class="story-card--body">
							<h4 class="story-card--title">3:12 Transformation</h4>
						</a>
					</div>

				</div>

			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="row">



					<div class="story-card story-card-large story-card-small-groups col">
						<span class="badge badge-secondary">Daily</span>
						<a href="/ministry/small-groups/" class="story-card--body">
							<h4 class="story-card--title">Community Groups</h4>
						</a>
					</div>

				</div>
			</div>
		</div>
		<div class="row text-center mt-5">
			<div class="col">
				<a href="/community" class="arrow-link">View all groups <i class="material-icons" style="font-size:1rem;">keyboard_arrow_right</i></a>
			</div>
		</div>
	</div>
</section>
