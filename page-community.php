<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<header class="page-hero">
	<div class="container">
	<div class="page-hero-content">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<?php echo do_shortcode("[types field='page-hero-intro'][/types]"); ?></div>
	</div>
	</div>
</header>

<section class="wrapper">
<div class="container pb-5" id="belong">
	<h2>Belong</h2>
		<?php echo do_shortcode("[wpv-view name='ministries']"); ?>
</div>
<div class="container pb-5 pt-5" id="grow">
	<h2>Grow</h2>
		<?php echo do_shortcode("[wpv-view name='ministries-grow']"); ?>
</div>
<div class="container pb-5 pt-5" id="go">
	<h2>Go</h2>
		<?php echo do_shortcode("[wpv-view name='ministries-go']"); ?>
</div>
</section>

<?php include 'cta-lets-connect.php' ?>

<?php get_footer(); ?>
